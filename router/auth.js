const router = require('express').Router();
const User = require('../models/Users');

const Joi = require('@hapi/joi');

const schema = {
  name: Joi.string().required(),
  email: Joi.string()
    .email()
    .required(),
  password: Joi.string().required(),
};

router.post('/register', async (req, res) => {
  console.info(req.body);
  const { name, password, email } = req.body;
  Joi.validate(req.body, schema);

  const user = new User({
    name,
    password,
    email,
  });

  try {
    const savedUser = await user.save();
    res.send(savedUser);
  } catch (e) {
    res.status(400).send(e);
  }
});

module.exports = router;
