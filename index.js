const express = require('express');
const app = express();
const mongoose = require('mongoose');
const authRouter = require('./router/auth');

mongoose.connect(
  'mongodb+srv://nperez:asd123@mflix-s1c8g.mongodb.net/auth',
  {
    useNewUrlParser: true,
  },
  () => console.info('conected to DB'),
);

app.use(express.json());

app.use('/api/user', authRouter);

app.listen(3000, () => console.info('Server listening at 3000'));
